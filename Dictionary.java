import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
/**
* Data class
*
* ----- DEFINITION data structure -----
*
* A data structure is organization of data to enable efficient storage and retreival. A choice of specific data
* structure depends on a use case. For example, if unique items need to be stored (for instance English language terms) 
* hash set or hash table are
* common choice. 
*
* ----- references -----
*
* https://www.javatpoint.com/data-structure-tutorial
*
* ----- compile -----
*
* javac Dictionary.java
*
* ----- run example -----
*
* java Dictionary dictionaryFile checkFile 
* 
*/
class Dictionary{
    public static void main(String args[]){
        String filea;
        String fileb;
        String tokena;
        Scanner reada;
        Scanner readb;
        HashSet<String> seta=new HashSet<String>();
        HashSet<String> setb=new HashSet<String>();
        filea=args[0]; // dictionary file
        fileb=args[1]; // file to check
        try{
            reada=new Scanner(new File(filea));
            readb=new Scanner(new File(fileb));
            for(;reada.hasNext();){ // read dictionary items
                seta.add(reada.next());
            }
            for(;readb.hasNext();){ // read check items
                setb.add(readb.next());
            }
            for(Object obja : setb){ // check if item in dictionary
                tokena=(String)obja;
                if(seta.contains(tokena)){
                    System.out.println(tokena+" is in the dictionary");
                } else {
                    System.out.println(tokena+" is not in the dictionary");
                }
            }
        }catch(IOException e){
        }
    }
}
